from flask import Flask, render_template, jsonify
import json
import requests
import random
from random import sample 
app = Flask(__name__)

API_ALLOSAURUS = "https://allosaurus.delahayeyourself.info/api/dinosaurs/"

@app.route('/', methods=['GET'])
def hello():
    
    response = requests.get(API_ALLOSAURUS)
    content = json.loads(response.content.decode('utf-8'))
    return render_template("template1.html", content = content) 

@app.route('/<string:nom>', methods=['GET'])
def bye(nom):
    
    response = requests.get(API_ALLOSAURUS + nom)
    test2 = requests.get(API_ALLOSAURUS).json()
    test = json.loads(response.content.decode('utf-8'))
    aleatoire = random.sample(test2, 3)
    return render_template("template2.html", test = test, aleatoire = aleatoire) 